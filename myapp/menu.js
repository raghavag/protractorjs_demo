

element(by.css(.sidebar-container#ng-app li a span)).getText().then(function(text){
			expect(text).toBe("Queues");
		});

element(by.css(ul li span)).getText().then(function(text){
			expect(text).toBe("Queues");
		});		



		//css chaining
		element(by.css('button#continue_button')).click();		
		//css child nodes
		element(by.css('table td a')).getText().then(function(text){
			console.log(text);
		});
		//css attributes
		element(by.css('[id="title"]')).getText().then(function(text){
			console.log(text);
		});