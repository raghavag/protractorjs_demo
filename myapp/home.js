var util = require ('util');
describe("Home page tests",function(){
	
	beforeEach(function(){
		browser.get("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/login");
		expect(browser.getCurrentUrl()).toEqual("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/login");		
	});
	var home_page=require('./login.js');
	it("Testing login page",function(){
		home_page.userName("admin");
		home_page.password("admin");
		home_page.submit("Submit");
	
	
	
element.all(by.css('span.menu-text')).filter(function(elem, index) {
  return elem.getText().then(function(text) {
    return text === 'Codes';
  });
}).then(function(filteredElements) {
  filteredElements[0].click();
  		
	browser.get("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/codes/codes");
	expect(browser.getCurrentUrl()).toEqual("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/codes/codes");
	var first = element.all(by.css('table tbody tr td')).first().click();
	expect(browser.getCurrentUrl()).toContain("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/codes/codes/12");	
	var textmsg="Professional Claims";
	element(by.model("code.description")).clear().sendKeys(textmsg);	
	element(by.buttonText("Save")).click();	
	browser.get("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/letters/recipientlist");
	expect(browser.getCurrentUrl()).toEqual("http://ec2-52-33-170-253.us-west-2.compute.amazonaws.com:8080/qpair/#/letters/recipientlist");
});
		
	});		
});
