describe("working on css",function(){
	
	
	beforeEach(function(){
		browser.get("http://www.thetestroom.com/jswebapp/");
		expect(browser.getCurrentUrl()).toEqual("http://www.thetestroom.com/jswebapp/");		
	});
	
	it("checking app with css",function(){
	// using css tag
	element(by.css('input')).sendKeys("hi");
	
	// using css class
		element(by.css(".ng-binding")).getText().then(function(text){
			expect(text).toEqual("hi");
		});	
	
	
	//using css id
	
	element(by.css('#continue_button')).click();
	
	//using css chaining
	
	element(by.css('button#continue_button')).click();
	
	// child nodes
	
	element(by.css('table tr td a')).getText().then(function(text){
		console.log(text);
		expect(text).toEqual("THETESTROOM.COM");
	});
});
});