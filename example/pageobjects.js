var util = require ('util');
describe("Test the home page for input feild",function(){
	
	beforeEach(function(){
		browser.get("http://www.thetestroom.com/jswebapp/");
		expect(browser.getCurrentUrl()).toEqual("http://www.thetestroom.com/jswebapp/");		
	});
	xit("validate inp and out using expect",function(){		
		element(by.model("person.name")).sendKeys("Hi Bhaskar");
		element(by.binding("person.name")).getText().then(function(text){
			expect(text).toEqual("Hi Bhaskar");
		});

		element(by.buttonText("CONTINUE")).click();
		element(by.model("animal")).$('[value="2"]').click();
		element.all(by.css(".ng-pristine option")).then(function(list){
			expect(list.length).toBe(4);
			expect(list[2].getText()).toBe("Simba the Lion");
		});
		element(by.buttonText("CONTINUE")).click();
		var thankYouText = element(by.css('h1')).getText();
		expect(thankYouText).toBe("Thank you");
	});	


	
	var home_page=require('./page1.js');
	it("validate inp and out",function(){	

		home_page.inputvalue("Hi Bhaskar");
		home_page.bindtext();
		home_page.clickcont();

	});	

	
});	